import Foundation

enum SocketError: Error {
    case sendError(errMsg: String)
    case receiveError(errMsg: String)
}
class SocketLayer: NSObject {
    
    
    var inputStream:InputStream!
    var outputStream: OutputStream!
    
    let maxReadLength = 1024
    
    func setupNetworkCommunication() {
        var readStream: Unmanaged<CFReadStream>?
        var writeStream: Unmanaged<CFWriteStream>?
        
        CFStreamCreatePairWithSocketToHost(kCFAllocatorDefault,
                                           "localhost" as CFString,
                                           8080,
                                           &readStream,
                                           &writeStream)
        
        inputStream = readStream!.takeRetainedValue()
        outputStream = writeStream!.takeRetainedValue()
        inputStream.schedule(in: .current, forMode: .common)
        outputStream.schedule(in: .current, forMode: .common)
        inputStream.open()
        outputStream.open()
    }
    
    public func sendMsg(msg:String) throws {
        let data = msg.data(using: .utf8)!
        _ = try data.withUnsafeBytes {
            guard let pointer = $0.baseAddress?.assumingMemoryBound(to: UInt8.self) else {
                throw SocketError.sendError(errMsg: "coundnt get pointer of data")
            }
            let status = outputStream.write(pointer, maxLength: data.count)
            if status < 0{
                throw SocketError.sendError(errMsg: "failed to write into stream")
            }
        }
    }
    public func receiveMsg() -> String{
        var buf = Array<UInt8>(repeating:0, count: 1024)
        inputStream.read(&buf, maxLength: maxReadLength)
        let output = NSString(bytes: &buf, length: maxReadLength, encoding: String.Encoding.utf8.rawValue)
        return output! as String
    }
}
