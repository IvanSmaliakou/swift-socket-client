import Foundation

extension Character
{
    func unicodeIndex() -> UInt32
    {
        let characterString = String(self)
        let scalars = characterString.unicodeScalars
        
        return scalars[scalars.startIndex].value
    }
}

enum EncryptorError: Error {
    case charNotFound(errMsg: String)
}

class Encryptor{
    let ALPHABET = "!! .,'ABCDEFHIGKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
    
    var module: Int64 = 0
    var e: Int64 = 0
    
    public func Encrypt(text:String) throws ->String{
        var res:String = ""
        for char in text {
            res.append(try EnctyptChar(ch: char))
        }
        return res
    }
    
    private func EnctyptChar(ch:Character) throws ->Character{
        let index = try Int64(GetAlphabetIndex(char: ch))
        let i = RecursevePower(n: index, pow: e, remainder: module) %  module
        return Array(ALPHABET)[Int(i)]
    }
    
    private func GetAlphabetIndex(char: Character) throws -> Int{
        guard let charIndex = ALPHABET.firstIndex(of: char) else {
            throw EncryptorError.charNotFound(errMsg: "coundn't find char")
        }
        
        return ALPHABET.distance(from: ALPHABET.startIndex, to: charIndex)
    }
    private func RecursevePower(n:Int64, pow:Int64, remainder:Int64)->Int64{
        if (pow == 1) {
            return n % remainder;
        }
        if (pow % 2 == 0) {
            return RecursevePower(n: n * n%remainder, pow: pow/2, remainder: remainder)
        }
        return n % remainder * RecursevePower(n: n * n % remainder, pow: pow / 2, remainder: remainder)
    }
}
