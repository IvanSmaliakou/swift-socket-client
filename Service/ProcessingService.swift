import Foundation

enum ProcessorError: Error {
    case parseError(msg: String)
}

class Processor {
    
    static public func parseResp(resp:String, into: Encryptor) throws -> Encryptor{
        into.module = try parseModule(resp: resp)
        let startIndex = resp.firstIndex(of: ",")
        into.e = try parseE(resp: resp, startIndex: startIndex!)
        return into
    }
    
    static private func parseModule(resp:String) throws -> Int64{
        if !resp.contains("module"){
            throw ProcessorError.parseError(msg: "couldnt parse response")
        }
        let st = resp.firstIndex(of: "{")
        let end = resp.firstIndex(of: "}")
        let rangeModule = st!..<end!
        let module = String(resp[rangeModule].dropFirst())
        return Int64(module) ?? 0
    }
    
    static private func parseE(resp:String, startIndex: String.Index) throws -> Int64{
        if !resp.contains(","){
            throw ProcessorError.parseError(msg: "couldnt parse response")
        }
        let range = startIndex..<resp.endIndex
        let str = resp[range]
        let st = str.firstIndex(of: "{")
        let end = str.firstIndex(of: "}")
        let range2 = st!..<end!
        return Int64(resp[range2].dropFirst()) ?? 0
    }
    
}
