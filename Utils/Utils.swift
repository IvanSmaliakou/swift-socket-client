import Foundation

public enum ResponceType: String {
    case DATA = "data"
    case HASH = "hash"
}

public enum AlertType{
    case ERROR
    case RESULT
}

func hashSum(text: String) -> Int{
    var hs: UInt32 = 0
    for char in text{
        hs += char.unicodeIndex()
    }
    return Int(hs)
}

func wrapWithResponceType(type: ResponceType, body: String) -> String {
    return "\(type.rawValue)=\(body)"
}

extension String{
    func equalString(other: String) -> Bool{
        let endIndex = other.endIndex
        let range = self.startIndex..<endIndex
        let this = self[range]
        if this.count != other.count{
            return false
        }
        return this == other
    }
}
