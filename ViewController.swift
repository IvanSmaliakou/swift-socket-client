import Cocoa

class ViewController: NSViewController {
    
    let sl = SocketLayer()
    
    @IBOutlet var textLabel: NSView!
    @IBOutlet weak var text: NSTextFieldCell!
    
    @IBOutlet weak var progressRing: NSProgressIndicator!
    
    var isIndicatorActive:Bool! = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        progressRing.isHidden=true
    }
    
    override var representedObject: Any? {
        didSet {
        }
    }
    
    
    @IBAction func ButtonClicked(_ sender: NSButtonCell) {
        progressRing.isHidden=false
        progressRing.startAnimation(nil)
        sl.setupNetworkCommunication()
        
        var enc = Encryptor()
        let receivedMessage: String
        do{
            try sl.sendMsg(msg: "openkey")
            let hash = hashSum(text: text.stringValue)
            let wrappedHash = wrapWithResponceType(type: ResponceType.HASH, body: String(hash))
            try sl.sendMsg(msg: wrappedHash)
            receivedMessage = sl.receiveMsg()
            enc = try Processor.parseResp(resp: receivedMessage, into: enc)
            let encryptedText = try enc.Encrypt(text: text.stringValue)
            let wrappedText = wrapWithResponceType(type: ResponceType.DATA, body: encryptedText)
            try sl.sendMsg(msg: wrappedText)
            let result = sl.receiveMsg()
            alert(message: result, type: AlertType.RESULT)
        }catch SocketError.sendError(let msg){
            alert(message: msg, type: AlertType.ERROR)
        }catch SocketError.receiveError(let msg){
            alert(message: msg, type: AlertType.ERROR)
        }catch ProcessorError.parseError(let msg){
            alert(message: msg, type: AlertType.ERROR)
        }catch EncryptorError.charNotFound(let msg){
            alert(message: msg, type: AlertType.ERROR)
        }catch {
            alert(message: "unexpected error", type: AlertType.ERROR)
        }
    }
    
    func alert(message: String, type: AlertType){
       let alert: NSAlert = NSAlert()
            if type == AlertType.RESULT{
                alert.messageText = "Результат"
                if message.equalString(other: "passed"){
                    alert.informativeText = "ЭЦП прошла проверку"
                    alert.alertStyle = NSAlert.Style.informational
                }else if message.equalString(other: "failed"){
                    alert.informativeText = "ЭЦП не прошла проверку"
                    alert.alertStyle = NSAlert.Style.critical
                }
            }else{
                alert.messageText = "Ошибка"
                alert.alertStyle = NSAlert.Style.critical
                alert.informativeText = message
            }
            
            alert.addButton(withTitle: "OK")
            let res = alert.runModal()
            if res == NSApplication.ModalResponse.alertFirstButtonReturn {
                exit(-1)
            }
        }

}
